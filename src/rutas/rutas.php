<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Slim\Http\UploadedFile;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

$app->add(new \Slim\Middleware\Session([
    'name' => 'session_fnsl',
    'autorefresh' => true,
    'lifetime' => '1 hour'
]));

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
});

//Obtener todos los productos
$app->get('/api/getproducts', function(Request $request, Response  $response) use($app){
    $sqlGetProductos = "SELECT * FROM producto";
    $sqlGetPhotos = "SELECT * FROM fotos_producto";

    try {
        $db = new db();
        $db = $db->conectDB();
        $productosRes = $db->prepare($sqlGetProductos);
        $productosRes->execute();

        if ($productosRes->rowCount() > 0) {
            $productos = $productosRes->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $mensaje = "No existen productos en la BD";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
            $db = null;
            $productosRes = null;
            return;
        }

        $photosRes = $db->prepare($sqlGetPhotos);
        $photosRes->execute();

        if ($photosRes->rowCount() > 0) {
            $photos = $photosRes->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $mensaje = "No existen fotos en la BD";
            $arrRes = array("ok" => false, "data" => $mensaje);
            echo json_encode($arrRes);
            $db = null;
            $productosRes = null;
            $photosRes = null;
            return;
        }
        
        $db = null;
        $productosRes = null;
        $photosRes = null;

        $photosTemp = array();
        $productosFinal = array();

        for ($i=0; $i < count($productos); $i++) { 
            $id_producto = $productos[$i]["id_producto"];
            for ($j=0; $j < count($photos); $j++) { 
                if ($photos[$j]["id_producto"] == $id_producto) {
                    array_push($photosTemp, $photos[$j]["ruta"]);
                }
            }
            $temp = array("id_producto" => $productos[$i]["id_producto"], "nombre" => $productos[$i]["nombre"], 
            "descripcion" => $productos[$i]["descripcion"], "precio" => $productos[$i]["precio"], "fotos" => $photosTemp);
            array_push($productosFinal, $temp);
            $temp = array();
            $photosTemp = array();
        }

        $res = array("ok" => true, "data" => $productosFinal);
        echo json_encode($res);

    } catch (Exception $e) {
        $arr = array("ok" => false, "data" => $e->getMessage());
        echo json_encode($arr);
    }
});



// Catch-all route to serve a 404 Not Found page if none of the routes match
// NOTE: make sure this route is defined last
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function ($req, $res) {
    $handler = $this->notFoundHandler; // handle using the default Slim page not found handler
    return $handler($req, $res);
});