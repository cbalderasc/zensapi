<?php
    class db {
        public function conectDB() {
            $dbHost = $_ENV['DB_HOST'];
            $dbUser = $_ENV['DB_USER'];
            $dbPass = $_ENV['DB_PASSWORD'];
            $dbName = $_ENV['DB_NAME'];

            $mysqlConnect = "mysql:host=$dbHost;dbname=$dbName;charset=UTF8";
            $dbConection = new PDO($mysqlConnect, $dbUser, $dbPass);
            $dbConection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $dbConection;
        }
    }